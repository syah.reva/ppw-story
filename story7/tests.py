from django.test import TestCase
from django.test import Client
from django.urls import resolve

from . import views

class UnitTestForStory7(TestCase):

    def test_func_page(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, views.story7)
