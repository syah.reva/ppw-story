$(document).ready(function($){
    $('.accordion_rectangle:first').addClass('activate')
    $('.accordion_rectangle:first').children('.acc_trigger').children('i').addClass('fa-chevron-down')
    $('.accordion_rectangle:first').addClass('activate')
    $('.accordion_rectangle:first').addClass('activate')


});

$(".reorder-up").click(function(){
    var $current = $(this).closest('.wrapper')
    var $previous = $current.prev('.wrapper');
    if($previous.length !== 0){
      $current.insertBefore($previous);
    }
    return false;
  });

  $(".reorder-down").click(function(){
    var $current = $(this).closest('.wrapper')
    var $next = $current.next('.wrapper');
    if($next.length !== 0){
      $current.insertAfter($next);
    }
    return false;
  });

$('.acc_trigger').click(function(e) {
  	e.preventDefault();
  
    var $this = $(this);
  
    if ($this.next().hasClass('show')) {
        $this.next().removeClass('show');
        $this.next().slideUp(350);
    } else {
        $this.parent().parent().find('p .acc_container').removeClass('show');
        $this.parent().parent().find('p .acc_container').slideUp(350);
        $this.next().toggleClass('show');
        $this.next().slideToggle(350);
    }
});