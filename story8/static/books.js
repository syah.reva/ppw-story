document.getElementById('search-button').addEventListener('click', bookSearch);

function bookSearch() {
	let search_query = document.getElementById('searchbar').value;
	document.getElementById('book-results').innerHTML = "";

	$.ajax({
		url: "https://www.googleapis.com/books/v1/volumes?q=" + search_query,
		dataType: "json",
		type: 'GET',
		success: (data) => {
      console.log(data);
			for(let i = 0; i < data.items.length; i++) {
				let title, image, authors;

				// set the title
				if(!data.items[i].volumeInfo.title) 
					title = "<span class='title'>No Title Available</span>";
				else
					title = "<span class='title'>" + data.items[i].volumeInfo.title + "</span>";

				// set the image
				if(!data.items[i].volumeInfo.imageLinks.thumbnail)
					image = "<img src='#' alt='no image available'>";
				else
					image = "<img src='" + data.items[i].volumeInfo.imageLinks.thumbnail + "' alt='no image available'>";

				// set the authors
				authors = data.items[i].volumeInfo.authors.join(", ");
				authors = "<span class='authors'>" + authors + "</span>";

				let block_left = "<div class='block-left'>" + image + "</div>";
				let block_right = "<div class='block-right'>" + title + authors + "</div>";
				let block = "<div class='result-block'>" + block_left + block_right;

				document.getElementById('book-results').innerHTML += block;
			}
		},
	});
}