import requests
import json
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.http import JsonResponse
from django.contrib import messages


# Create your views here.

def story8(request):
    response = {}
    return render(request, 'story8.html', response)

def database(request):
    arguments = request.GET['q']
    url_terkait = 'https://www.googleapis.com/books/v1/volumes?q=' + arguments
    url = requests.get(url_terkait)
    data = json.loads(url.content)
    return JsonResponse(data, safe=False)

