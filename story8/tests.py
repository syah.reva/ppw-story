from django.test import TestCase
from django.test import Client
from django.urls import resolve

from . import views

class UnitTestForStory8(TestCase):

    def test_func_page(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, views.story8)

    def test_func_page2(self):
        found = resolve('/database/')
        self.assertEqual(found.func, views.database)

