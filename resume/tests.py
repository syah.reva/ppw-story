from django.test import TestCase
from django.test import Client
from django.urls import resolve

from . import views

# Create your tests here.
class UnitTestForStory7(TestCase):


    def test_func_page(self):
        found = resolve('/resume/')
        self.assertEqual(found.func, views.resume)

