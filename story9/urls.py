from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('login/', views.masuk, name = 'login'),
    path('signup/', views.baru, name='signup'),
    path('logout/', views.keluar, name='logout'),
]
