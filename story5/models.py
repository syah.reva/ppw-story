from django.db import models

# Create your models here.

class Models(models.Model):
    course_name = models.CharField(max_length = 30)
    lecture_time = models.CharField(max_length=30)
    room = models.CharField(max_length=30)

    def __str__(self):
        return "{}.{}".format(self.id, self.course_name)
