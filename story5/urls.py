from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('delete/<int:id>', views.course_delete, name='course_delete'),
    path('story5/', views.college_course, name='college_course'),
    path('', views.registration_form, name='registration_form')
]
