from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import ModelsForm
from .models import Models
from django.contrib import messages

# Create your views here.
def registration_form(request):
    post_form = ModelsForm(request.POST or None)

    if request.method == "POST":
        if post_form.is_valid():
            post_form.save()
        return redirect('story5:college_course')
    data_form = {
        'post_form' : post_form
    }
    return render(request, 'create.html', data_form)

def college_course(request):
    list_form = Models.objects.all()
    data_form = {
        'list_form' : list_form
    }
    print(list_form)
    return render(request, 'college_course.html', data_form)


def course_delete(request, id):
    Models.objects.filter(pk=id).delete()
    return redirect('story5:college_course')
