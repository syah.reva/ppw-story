from django import forms
from .models import Models
from django.forms import ModelForm

class ModelsForm(forms.ModelForm):
    class Meta :
        model = Models
        fields = [
            'course_name',
            'lecture_time',
            'room',    
        ]

        widgets = {
            'course_name':forms.TextInput(attrs={'class':'form-control','placeholder':'Enter the course name'}),
            'lecture_time':forms.TextInput(attrs={'class':'form-control','placeholder':'Enter the lecture time'}),
            'room':forms.TextInput(attrs={'class':'form-control','placeholder':'Enter the room name'}),
        }
