from django.test import TestCase
from django.test import Client
from django.urls import resolve

from .models import Kegiatan, Peserta
from . import views


# Create your tests here.

class UnitTestForStory6(TestCase):

    def test_func_page(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, views.activity)

    def test_story6_model_kegiatan(self):
        Kegiatan.objects.create(nama='Test Kegiatan')
        kegiatan = Kegiatan.objects.get(nama='Test Kegiatan')
        self.assertEqual(str(kegiatan), 'Test Kegiatan')

    def test_story6_kegiatan_form_invalid(self):
        Client().post('/story6/post_kegiatan', data={})
        jumlah = Kegiatan.objects.filter(nama='Unit Test').count()
        self.assertEqual(jumlah, 0)
