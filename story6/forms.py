from django import forms



class KegiatanForm(forms.Form):
    nama_kegiatan = forms.CharField(label = 'Activity Name',max_length = 30, widget = forms.TextInput(attrs = {'class': 'form-control', 'placeholder':'Enter the activity name'}))


class PesertaForm(forms.Form):
    nama_peserta = forms.CharField(label = 'Participant Name', max_length = 30, widget = forms.TextInput(attrs = {'class': 'form-control', 'placeholder':'Enter the participant name'}))
